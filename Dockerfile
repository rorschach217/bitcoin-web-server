FROM node:8.9.4

RUN useradd -ms /bin/bash everything_crypto

USER everything_crypto

WORKDIR /usr/src/app

COPY package.json development.json ./

COPY ./dist .
