/**
 * Created by rorschach217 on 25/07/17.
 */
import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";
import * as express from "express";
import * as logger from "morgan";
import * as path from "path";
import errorHandler = require("errorhandler");
import methodOverride = require("method-override");
import mongoose = require("mongoose"); //import mongoose
import * as exphbs from 'express-handlebars';

//routes
import { IndexRoute } from "./routes/index";

//interfaces

//models
import { IModel } from "./models/model"; //import IModel

//schemas


/**
 * The server.
 *
 * @class Server
 */
export class Server {

    public app: express.Application;

    private model: IModel; //an instance of IModel

    /**
     * Bootstrap the application.
     *
     * @class Server
     * @method bootstrap
     * @static
     * @return {ng.auto.IInjectorService} Returns the newly created injector for this app.
     */
    public static bootstrap(): Server {
        return new Server();
    }

    /**
     * Constructor.
     *
     * @class Server
     * @constructor
     */
    constructor() {
        //instance defaults
        this.model = Object(); //initialize this to an empty object

        //create expressjs application
        this.app = express();
        // this.app.use((req, res, next)=>{
          // res.header("Access-Control-Allow-Origin", "*");
          // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
          // next();
        // })
        //configure application
        this.config();

        //add routes
        this.routes();

        //add api
        this.api();

        console.log("Server Started");
    }

    /**
     * Create REST API routes
     *
     * @class Server
     * @method api
     */
    public api() {
        //empty for now
    }

    /**
     * Configure application
     *
     * @class Server
     * @method config
     */
    public config() {
        // const MONGODB_CONNECTION: string = "mongodb://localhost:27017/bitcoin-web";

        //add static paths
        this.app.use(express.static(path.join(__dirname, "public")));

        //configure pug
        this.app.set("views", path.join(__dirname, "views"));
        this.app.engine('handlebars', exphbs({defaultLayout: 'main'}))
        this.app.enable('view cache');
        this.app.set("view engine", "handlebars");

        //use logger middlware
        this.app.use(logger("dev"));

        //use json form parser middlware
        this.app.use(bodyParser.json());

        //use query string parser middlware
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));

        //use cookie parker middleware middlware
        this.app.use(cookieParser("1QAZXSW23EDCVFR45TGBNHY67UJMKI89OLP0"));

        //use override middlware
        this.app.use(methodOverride());

        //use q promises
        global.Promise = require("bluebird").Promise;
        mongoose.Promise = global.Promise;

        //connect to mongoose
        // let connection: mongoose.Connection = mongoose.createConnection(MONGODB_CONNECTION);

        //create models
        // this.model.video = connection.model<IVideoModel>("Video", VideoSchema);

        //catch 404 and forward to error handler
        this.app.use(function(err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
            err.status = 404;
            next(err);
        });

        //error handling
        this.app.use(errorHandler());
    }

    /**
     * Create router
     *
     * @class Server
     * @method api
     */
    public routes() {
        let router: express.Router;
        router = express.Router();

        //IndexRoute
        IndexRoute.create(router);

        //VideoRoute
        // VideoRoute.create(router, this.model.video);

        //use router middleware
        this.app.use(router);
    }
}
