import * as fs from 'fs';

let Config = function() {
  this.configFile = process.env.NODE_ENV == 'production' ? `${process.env.PWD}/production.json` : `${process.env.PWD}/development.json`
  this.content = fs.readFileSync(this.configFile);
  this.getConfig = (key) => {
    let buffer = new Buffer(this.content)
    let jsonData = JSON.parse(buffer.toString());
    return key ? jsonData[key] : jsonData
  }
}

export const config = new Config();
