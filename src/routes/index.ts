/**
 * Created by rorschach217 on 25/07/17.
 */
import { NextFunction, Request, Response, Router } from "express";
import { BaseRoute } from "./route";
import { config } from '../config/config';
let BitcoinClient =  require('bitcoin-core');

/**
 * / route
 *
 * @class User
 */
export class IndexRoute extends BaseRoute {
    public title= "";
    public bitcoinOptions = config.getConfig('bitcoin')
    public client = new BitcoinClient(this.bitcoinOptions);
    /**
     * Create the routes.
     *
     * @class IndexRoute
     * @method create
     * @static
     */
    public static create(router: Router) {
        //log
        console.log("[IndexRoute::create] Creating index route.");

        //add home page route
        router.get("/", (req: Request, res: Response, next: NextFunction) => {
            new IndexRoute().index(req, res, next);
        });

        //generate bitcoin post page
        router.get("/bitcoin/generate/address", (req: Request, res: Response, next: NextFunction) => {
            new IndexRoute().generateBitcoinAddress(req, res, next);
        });

        //get balance
        router.get("/bitcoin/balance", (req: Request, res: Response, next: NextFunction) => {
            new IndexRoute().getBitcoinBalance(req, res, next);
        });

        //send bitcoin
        router.post("/bitcoin/send/bitcoin", (req: Request, res: Response, next: NextFunction) => {
            new IndexRoute().sendBitcoin(req, res, next);
        });

        //get balance of specific address
        router.post("/bitcoin/balance", (req: Request, res: Response, next: NextFunction) => {
            new IndexRoute().getBitcoinBalanceOfAddress(req, res, next);
        });

        router.get("/bitcoin/unspent/list", (req: Request, res: Response, next: NextFunction) => {
            new IndexRoute().getUnspentList(req, res, next);
        });


    }

    /**
     * Constructor
     *
     * @class IndexRoute
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * The home page route.
     *
     * @class IndexRoute
     * @method index
     * @param req {Request} The express Request object.
     * @param res {Response} The express Response object.
     * @next {NextFunction} Execute the next method.
     */
    public index(req: Request, res: Response, next: NextFunction) {
        //set custom title
        this.title = "Home | Bitcoin transaction Server";

        //set options
        let options: Object = {
            "message": "Welcome to the Bitcoin Transaction Server !",
            "layout": 'main'
        };

        //render template
        res.render("index", options);
    };

    public generateBitcoinAddress(req: Request, res: Response, next: NextFunction) {
      //set options
      this.client.command([{ method: 'getnewaddress', parameters: [] }]).then((response)=>{
        process.env.NODE_ENV != 'production' ? console.log("response = ",response) : null;
        let options: Object = {
            "address": response[0],
            "layout": 'main'
        };
        res.render("address", options);
      });
      // res.render("address", {"address": 'this is randome generated address. djJjdjsJO3PoqeoirsPEOdoiwroi',"layout": 'main'});
    }

    public getBitcoinBalance(req: Request, res: Response, next: NextFunction) {
      //set options
      this.client.command([{ method: 'getbalance', parameters: [] }]).then((response)=>{
        process.env.NODE_ENV != 'production' ? console.log("response = ",response) : null;
        let options: Object = {
            "balance": response[0],
            "layout": 'main'
        };
        res.render("balance", options);
      });
      // res.render("balance", {"balance": '0.00000000',"layout": 'main'});
    }

    public sendBitcoin(req: Request, res: Response, next: NextFunction) {
      //set options
      let parameters = [req.body.address, parseFloat(parseFloat(req.body.amount).toFixed(8)), "sent from web", `amount sent ${req.body.amount}`]
      this.client.command([{ method: 'sendtoaddress', parameters: parameters }]).then((response)=>{
        process.env.NODE_ENV != 'production' ? console.log("response = ",response) : null;
        let options: Object = {
            "transaction": response[0],
            "layout": 'main'
        };
        res.render("sent", options);
      });
      // res.render("sent", {"transaction": 'woeiuhqwinkldsnverhijnlkajsncpiruojankasndoiguhritkasdmnoasjnriaruandkasfouagsoijfnlkjfnvlakbrgiasdnfasndofiashfjnaslkmnlakrb',"layout": 'main'});
    }

    public getBitcoinBalanceOfAddress(req: Request, res: Response, next: NextFunction) {
      //set options
      this.client.command([{ method: 'getbalance', parameters: [req.body.address] }]).then((response)=>{
        process.env.NODE_ENV != 'production' ? console.log("response = ",response) : null;
        let options: Object = {
            "balance": response[0],
            "layout": 'main'
        };
        res.render("balance", options);
      });
      // res.render("balance", {"balance": '0.00000000',"layout": 'main'});
    }

    public getUnspentList(req: Request, res: Response, next: NextFunction) {
      //set options
      this.client.command([{ method: 'listunspent', parameters: [0] }]).then((response)=>{
        process.env.NODE_ENV != 'production' ? console.log("response = ",response) : null;
        let options: Object = {
            "transactions": response[0],
            "layout": 'main'
        };
        res.render("unspent", options);
      });
      // res.render("balance", {"balance": '0.00000000',"layout": 'main'});
    }
}
