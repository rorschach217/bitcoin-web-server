#!/usr/bin/env bash
pm2 stop www;
git pull;
npm run grunt;
export PORT=8000;
export NODE_ENV=development;
pm2 start ./bin/www.js;
