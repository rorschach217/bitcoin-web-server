"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const route_1 = require("./route");
const config_1 = require("../config/config");
let BitcoinClient = require('bitcoin-core');
class IndexRoute extends route_1.BaseRoute {
    constructor() {
        super();
        this.title = "";
        this.bitcoinOptions = config_1.config.getConfig('bitcoin');
        this.client = new BitcoinClient(this.bitcoinOptions);
    }
    static create(router) {
        console.log("[IndexRoute::create] Creating index route.");
        router.get("/", (req, res, next) => {
            new IndexRoute().index(req, res, next);
        });
        router.get("/bitcoin/generate/address", (req, res, next) => {
            new IndexRoute().generateBitcoinAddress(req, res, next);
        });
        router.get("/bitcoin/balance", (req, res, next) => {
            new IndexRoute().getBitcoinBalance(req, res, next);
        });
        router.post("/bitcoin/send/bitcoin", (req, res, next) => {
            new IndexRoute().sendBitcoin(req, res, next);
        });
        router.post("/bitcoin/balance", (req, res, next) => {
            new IndexRoute().getBitcoinBalanceOfAddress(req, res, next);
        });
        router.get("/bitcoin/unspent/list", (req, res, next) => {
            new IndexRoute().getUnspentList(req, res, next);
        });
    }
    index(req, res, next) {
        this.title = "Home | Bitcoin transaction Server";
        let options = {
            "message": "Welcome to the Bitcoin Transaction Server !",
            "layout": 'main'
        };
        res.render("index", options);
    }
    ;
    generateBitcoinAddress(req, res, next) {
        this.client.command([{ method: 'getnewaddress', parameters: [] }]).then((response) => {
            process.env.NODE_ENV != 'production' ? console.log("response = ", response) : null;
            let options = {
                "address": response[0],
                "layout": 'main'
            };
            res.render("address", options);
        });
    }
    getBitcoinBalance(req, res, next) {
        this.client.command([{ method: 'getbalance', parameters: [] }]).then((response) => {
            process.env.NODE_ENV != 'production' ? console.log("response = ", response) : null;
            let options = {
                "balance": response[0],
                "layout": 'main'
            };
            res.render("balance", options);
        });
    }
    sendBitcoin(req, res, next) {
        let parameters = [req.body.address, parseFloat(parseFloat(req.body.amount).toFixed(8)), "sent from web", `amount sent ${req.body.amount}`];
        this.client.command([{ method: 'sendtoaddress', parameters: parameters }]).then((response) => {
            process.env.NODE_ENV != 'production' ? console.log("response = ", response) : null;
            let options = {
                "transaction": response[0],
                "layout": 'main'
            };
            res.render("sent", options);
        });
    }
    getBitcoinBalanceOfAddress(req, res, next) {
        this.client.command([{ method: 'getbalance', parameters: [req.body.address] }]).then((response) => {
            process.env.NODE_ENV != 'production' ? console.log("response = ", response) : null;
            let options = {
                "balance": response[0],
                "layout": 'main'
            };
            res.render("balance", options);
        });
    }
    getUnspentList(req, res, next) {
        this.client.command([{ method: 'listunspent', parameters: [0] }]).then((response) => {
            process.env.NODE_ENV != 'production' ? console.log("response = ", response) : null;
            let options = {
                "transactions": response[0],
                "layout": 'main'
            };
            res.render("unspent", options);
        });
    }
}
exports.IndexRoute = IndexRoute;
